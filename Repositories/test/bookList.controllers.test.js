const app = require('../../routes/index');
const request = require('request');
const booklist = require('../bookList.repositories');
const bookList = new booklist();

test('Get Book Details', async () => {
  let id = 3;
  let result = await bookList.getBookDetails(id);
  
  expect(result).not.toBeNull();
  expect(result).toHaveProperty('title');
  expect(result).toHaveProperty('description');
  expect(result).toHaveProperty('price');
  expect(result).toHaveProperty('year');
  expect(result).toHaveProperty('buy_link');
  expect(result).toHaveProperty('img_source');
  
});



test('Get BookList', async () => {
  
  let result = await bookList.getBooklist();
  
   expect(result).not.toBeNull();
  
});