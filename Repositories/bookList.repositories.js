const mysql = require('../db/mysql');
const db = new mysql();

class BookListRepo{
     getBooklist(){
        return new Promise(async(resolve,reject)=>{
            try{
                const query = "Select * from book_details";
                const params = [];
                let result = await  db.execute(query,params);      
                resolve(result);
            }catch(e){
                reject(e);
            }
        })
    }

    getBookDetails(book_id){
        return new Promise(async(resolve,reject)=>{
            try{
                const query = "select * from book_details where id = ?";
                const params = [book_id];                
                let result = await  db.execute(query,params);
                resolve (result[0]);
            }catch(e){
                reject (e);
            }            
        })
    }
}
module.exports = BookListRepo;