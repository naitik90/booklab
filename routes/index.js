var express = require('express');
var router = express.Router();
const bookList = require('../Controllers/bookList.controllers');
/* GET home page. */
const booklist = new bookList(); 
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/getbooklist',booklist.getBookList);
router.get('/getbookdetails/:id',booklist.getBookDetail);

module.exports = router;
