
const bookList_repo = require('../Repositories/bookList.repositories');
const bookrepo = new bookList_repo();    

class bookList{
    
    async getBookList(req,res){
        try{
            
            const getBooksList = await bookrepo.getBooklist();
            res.send({
                status:200,
                data:getBooksList
            });
        }catch(e){
            
            res.send({status:500,
                    error:e.stack});
        }
        
    }
    async getBookDetail(req,res){
        try{
            const book_id = req.params.id;
            if(book_id){
                const getBookDetails = await bookrepo.getBookDetails(book_id);
                res.send({
                    status:200,
                    data:getBookDetails
                });
            }  
            else{
                res.send({
                    status:204,
                    error:"No Book id present"
                });
            }         
        }catch(e){
            res.send({
                status:500,
                error:e.stack
            });
        }
    }
}
module.exports = bookList;