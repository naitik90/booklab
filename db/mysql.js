var mysql      = require('mysql');
const config = require('config');
class mysqldb{
    constructor(){
        this.pool = mysql.createPool({
            connectionLimit : 10,

            host     : config.get("db.host"),
            user     : config.get("db.username"),
            password : config.get("db.password"),
            database : config.get("db.database")
          });
          
    }
    execute(query,param){
        return new Promise((resolve,reject)=>{
            try{
                
                this.pool.query(query,param, function (error, results, fields) {
                if (error) reject(error);
                    resolve(results);
                });
                
            }catch(e){
                reject (e);
            }
        })
        
        
    }
}
 

module.exports = mysqldb;